# useR! 2022

[https://user2022.r-project.org/program/posters/#posters-on-r-in-the-wild](https://user2022.r-project.org/program/posters/#posters-on-r-in-the-wild)

## Poster

* {VMR} Virtual Machines for/with R
  * [HAL](https://hal.archives-ouvertes.fr/hal-03701831v1)
  * [Poster](JF_REY_UserR2022_VMR.pdf)
